const Realm = require('realm')

export const Task = new Realm({
    schema: [{
        name: 'Task',
        properties: {name: 'string'}
    }]
})