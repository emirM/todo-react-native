import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Text
} from 'react-native'

import { Router, Scene } from 'react-native-router-flux'

import Home from './components/Home'
import Task from './components/Task'

export default class Index extends Component {
    render () {
        return (
            <Router>
                <Scene key="root">
                    <Scene key="home" component={Home} title="Главная" initial={true}></Scene>
                    <Scene key="task" component={Task} title="Задача"></Scene>
                </Scene>
            </Router>
        )
    }
}