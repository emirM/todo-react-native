import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet
} from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class TaskDetail extends Component {
    render () {
        const goBack = () => Actions.home()
        return (
            <View>
                <Text onPress={goBack}>Back</Text>
            </View>
        )
    }
}