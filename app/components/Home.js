import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    ScrollView
} from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class Home extends Component {
    render () {
        const goToTask = () => Actions.task({text: 'Hello task!'})
        return (
            <ScrollView>
                <View style={{marginTop: 108}}>
                    <Text onPress={goToTask}>This is Home</Text>
                </View>
            </ScrollView>
        )
    }
}