import React, {Component} from "react"
import {
    Text,
    View,
    StyleSheet
} from 'react-native'

import {connect} from 'react-redux'
import { Router, Scene } from 'react-native-router-flux'

import { Home } from '../components/Home'
import { TaskDetail } from '../components/TaskDetail'
import {addTask, removeTask} from "../actions/taskActions"

import Task from '../models/Task'

export class App extends Component {
    constructor() {
        super();
    }

    render() {
        var tasks = Task.objects('Task')
        return (
            <Router>
                <Scene key="root">
                    <Scene key="home" 
                        component={Home} 
                        title="Главная" 
                        initial={true} 
                        tasks={tasks}></Scene>
                    <Scene key="task" 
                        component={TaskDetail} 
                        title="Задача"
                        addTask={() => this.props.addTask("Name") } 
                        ></Scene>
                </Scene>
            </Router>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tasks: state.tasks,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addTask: (name) => {
            dispatch(addTask(name))
        },
        removeTask: () => {
            dispatch(removeTask())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)