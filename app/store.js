import {createStore, combineReducers, applyMiddleware} from "redux"
import logger from "redux-logger"
import thunk from "redux-thunk"

import task from "./reducers/taskReducer"

export default createStore(
    combineReducers({
        task
    }), 
    {}, 
    applyMiddleware(logger(), thunk)
)