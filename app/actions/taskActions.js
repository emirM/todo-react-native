import * as types from './types'
import Task from '../models/Task'

export function addTask(name) {
    return {
        type: types.ADD,
        payload: Task.write(() => {
            Task.create('Task', {name: name})
        })
    }
}

export function removeTask(task) {
    return {
        type: types.DELETE,
        payload: Task.write(() => {
            Task.delete(task)
        })
    }
}